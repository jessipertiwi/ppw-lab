Mandatory
1. Session: Login & Logout
a. Implementasi fungsi Login dan logout
Membuat urls.py yang mengecek udh login apa blm, kalo udh ke profile, kalo blm ke hal login. Hal login memanggil menuliskan pattern urls menuju halaman login dan logout
	Membuat csui_helper untuk membantu login 
Custom auth set dengan menggunakan session
	Membuat fungsi verifikasi user dan mengambil data user
       Membuat api_enterkomputer.py sebagai pemanggilan daftar drone dan lainnya
	Membuat berkas custom_auth untuk fungsi auth_login dan logout
	Membuat views.py untuk menampilkannya ke html
2. Session: Kelola Favorit
a. Implementasi fungsi "Favoritkan" untuk Drones
Saat tombol favoritkan ditekan, akan memanggil fungsi views add_session_drones dan tombol berubah menjadi hapus dari favorit
b. Implementasi fungsi "Hapus dari favorit" untuk Drones
Saat tombol favoritkan ditekan, akan memanggil fungsi views add_session_drones dan tombol berubah menjadi hapus dari favorit
c. Implementasi fungsi "Reset favorit" untuk Drones
Saat tombol Reset favorit ditekan, akan memanggil fungsi views clear_session_drones dan semua drones yang sudah difavoritkan akan kembali menjadi seperti semula (belum difavorit)
3. Cookies: Login & Logout
a. Implementasi fungsi Login
Halaman login akan mengambil username dan password, dan akan disamakan dengan yang sudah disimpan dengan memanggil method my_cookie_auth 
b. Implementasi fungsi Logout
Logout = clear cookie. Cookie soal login yang sebelumnya pada login cookie akan dihapus dan halaman akan redirect ke halaman login cookie.
4. Implementasi Header dan Footer
a. Buatlah header yang berisi tombol Logout hanya jika sudah login (baik pada session dan cookies). Buatlah sebagus dan semenarik mungkin.
Membuat header untuk halaman login session-cookie, profile session-cookie. Lalu meng extend pada masing2 halaman login dan profil.
5. Pastikan kalian memiliki Code Coverage yang baik
6. Jika kalian belum melakukan konfigurasi untuk menampilkan Code Coverage di Gitlab maka lihat langkah Show Code Coverage in Gitlab di README.md
7. Pastikan Code Coverage kalian 100%

Challenge
1. Implementasi API Optical dan SoundCard
a. Menambahkan link ke tab Optical dan Soundcard pada halaman Session Profile
Menambahkan link seperti pada drones dengan menambahkan file tables di tab panes profile.html.
b. Membuat tabel berisi data optical/soundcard 
Dalam folder table, membuat table untuk soundcards dan opticals. Isinya sama dengan drones, dengan ref dari opticals/souncards.
2. Implementasi fungsi umum yang sudah disediakan mengelola session:
a. Menggunakan fungsi umum untuk menambahkan (Favoritkan) optical/soundcard ke session
Memakai add_session_item pada tabel optical dan soundcard dengan ref urls memanggil yang opticals/soundcards (file table.html)
b. Menggunakan fungsi umum untuk menghapus (Hapus dari Favorit) optical/soundcard dari session 
Memangil del_session_item pada tabel optical dan soundcard dengan ref urls memanggil yang opticals/soundcards (file table.html)
c. Menggunakan fungsi umum untuk menghapus/reset kategori (drones/optical/soundcard) dari session.
Memakai clear_session_item item pada tabel optical dan soundcard dengan ref urls memanggil yang opticals/soundcards (file table.html)
3. Implementasi session untuk semua halaman yang telah dibuat pada Lab Sebelumnya
a. Jika halaman lab diakses tanpa login terlebih dahulu, maka mereka akan ditampilkan halaman login
b. Ketika halaman Lab ke-N diakses tanpa login, maka setelah login, pengguna akan diberikan tampilan Lab ke-N
c. Ubahlah implementasi csui_helper.py pada Lab 9 sehingga bisa digunakan oleh Lab 7 (Kalian boleh menghapus berkas csui_helper.py yang ada di Lab 7)
