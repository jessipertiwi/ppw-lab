Membuat halaman untuk Login menggunakan OAuth
1. Mendaftarkan aplikasi ke facebook developer page
a. Buka https://developers.facebook.com/
b. Daftar Sebagai Pengembang Facebook
c. Buat Aplikasi baru dengan mengklik tombol Add a New App dapat ditemukan di kanan atas
d. Masuk ke halaman aplikasi yang sudah dibuat dan pilih Add Product, kemudian tambahkan Facebook Login
e. Pada bagian setting silakan tambahkan platform dan pilih website, lalu sesuaikan isi dari site url. 
f. Cara lain: Anda mungkin juga mendapati halaman Quickstart, pilih Web sebagai platform.
g. Applikasi telah berhasil didaftarkan.

2. Melakukan OAuth Login menggunakan Facebook
a. Membuat file lab8.js yang berisi fungsi yang menginisiasi dan meload ke app facebook kita dengan keterangan appId terhadap javascript sdk secara asynchronuos.
b. Isi appId dan version dari yang tertera pada FB developer Page.
c. Mengimplementasi fungsi getLoginStatus pada lab8.js agar jika web dibuka dan sudah login maka akan langsung ditampilkan view sudah login.
d. Mengimplementasi fungsi render yang menerima parameter loginFlag untuk menentukan membuat halaman html yang sudah login atau belum. 
Jika sudah login, akan menampilkan segala info tampilan profil, form input post, tombol post status, dan tombol logout. 
Jika belum, akan menampilkan halaman berisi tombol login.
e. Menambahkan tombol login facebook pada lab_8.html yang jika diklik akan memanggil method facebookLogin().
f. Menambahkan fungsi facebookLogin() pada lab8.js. Fungsi facebookLogin() tersebut akan memanggil fungsi login dari instance FB (instance dari facebook SDK) yang berfungsi untuk menguthentifikasi dan mengauthorisasi user, function FB.login() ini juga memiliki parameter scope yang berguna untuk mengatur jenis permission apa saja yang aplikasi kita inginkan, sebagai contoh di atas kita telah menambah permission public_profile agar aplikasi kita dapat mengakses id, name, first_name, last_name dll milik kita.
3. Menampilkan informasi dari user yang login menggunakan API Facebook.
a. Menampilkan informasi user menggunakan Graph API yang dimiliki facebook, dengan menambahkan fungsi getUserData() untuk mendapatkan informasi user.
4. Melakukan post status facebook
a. Pada fungsi facebookLogin() di lab8.js tambahkan permission dengan menulis user_posts dan publish_actions dalam scope.
b. Graph API yang digunakan oleh facebook membuat kita bisa memposting status di facebook. Contohnya dengan potongan kode berikut :
function postFeed(){
     var message = "Hello World!";
     FB.api('/me/feed', 'POST', {message:message});
 }
5. Menampilkan post status pada halaman lab_8.html
a. (cek2 sublime)
6. Melakukan Logout
a. Membuat method logout dari instance facebook pada 
7. Implementasi css yang indah dan responsive

Jawablah pertanyaan yang ada di dokumen ini dengan menuliskannya pada buku catatan atau pada source code kalian yang dapat ditunjukkan saat demo
1. Apakah yang dimaksud dengan fungsi callback?
2. Dengan menambahkan permission user_posts dan publish_actions, apa saja yang dapat dilakukan oleh aplikasi kita menggunakan Graph API?
3. 

Pastikan kalian memiliki Code Coverage yang baik



 Jika kalian belum melakukan konfigurasi untuk menampilkan Code Coverage di Gitlab maka lihat langkah Show Code Coverage in Gitlab di README.md


 Pastikan Code Coverage kalian 100%





Additional


Melakukan delete status pada halaman facebook



 Implementasi tombol delete pada daftar post status

 Melakukan delete post status dengan menggunakan API Facebook yang ada.
